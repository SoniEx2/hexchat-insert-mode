/*
 * "Insert Mode" for HexChat
 * Copyright (C) 2018 Soni L.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
// plugin boilerplate
#[macro_use]
extern crate hexchat_plugin;
use hexchat_plugin::{Plugin, PluginHandle};

use std::sync::Arc;
use std::sync::atomic;
use std::sync::atomic::AtomicBool;
use std::sync::Mutex;

const KEY_ESCAPE: &str = "65307"; // 0xff1b
const KEY_RETURN: &str = "65293"; // 0xff0d
const KEY_ENTER: &str = "65421"; // 0xff8d

#[derive(Default)]
struct InsertModePlugin {
    cmd_i: Mutex<Option<hexchat_plugin::CommandHookHandle>>,
    key_press: Mutex<Option<hexchat_plugin::PrintHookHandle>>,
    enabled: Arc<AtomicBool>,
}

impl Plugin for InsertModePlugin {
    fn init(&self, ph: &mut PluginHandle, arg: Option<&str>) -> bool {
        ph.register(env!("CARGO_PKG_NAME"), env!("CARGO_PKG_DESCRIPTION"), env!("CARGO_PKG_VERSION"));
        ph.print(&format!("{} loaded", env!("CARGO_PKG_NAME")));
        {
            let enabled = self.enabled.clone();
            let cmd_i = ph.hook_command("i", move |ph, word, word_eol| {
                enabled.store(true, atomic::Ordering::Relaxed);
                hexchat_plugin::EAT_ALL
            }, 0, Some("Enters insert mode."));
            *self.cmd_i.lock().unwrap() = Some(cmd_i);
        }
        {
            let enabled = self.enabled.clone();
            let key_press = ph.hook_print("Key Press", move |ph, word| {
                if word[0] == KEY_ESCAPE && enabled.load(atomic::Ordering::Relaxed) {
                    enabled.store(false, atomic::Ordering::Relaxed);
                    hexchat_plugin::EAT_ALL
                } else  {
                    if word[0] == KEY_RETURN || word[0] == KEY_ENTER {
                        let input = if let Some(input) = ph.get_info(&hexchat_plugin::InfoId::Inputbox) {
                            input
                        } else {
                            return hexchat_plugin::EAT_NONE
                        };
                        if enabled.load(atomic::Ordering::Relaxed) {
                            if input.starts_with("/") {
                                ph.ensure_valid_context(|ph| {
                                    ph.command(&format!("settext /{}", input))
                                });
                            }
                        } else {
                            if !input.starts_with("/") {
                                ph.ensure_valid_context(|ph| {
                                    ph.command(&format!("settext /{}", input))
                                });
                            }
                        }
                    }
                    hexchat_plugin::EAT_NONE
                }
            }, 0);
            *self.key_press.lock().unwrap() = Some(key_press);
        }
        true
    }
}

hexchat_plugin!(InsertModePlugin);
